
import headerStyles from '../styles/Header.module.css'

const Header = () => {
  return (
    <div>
      <h1 className={headerStyles.title}>
        <span>Nextjs</span> Demo
      </h1>
      <p className={headerStyles.description}>
        Articles listed below
      </p>
    </div>
  )
}

export default Header